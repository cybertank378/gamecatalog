package io.rahman.games.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.rahman.games.BuildConfig
import io.rahman.games.core.Resource
import io.rahman.games.data.local.LatestGameDao
import io.rahman.games.data.local.TopGameDao
import io.rahman.games.data.local.model.GameDbModel
import io.rahman.games.data.local.model.Games
import io.rahman.games.data.remote.ApiInterface
import io.rahman.games.data.remote.model.GamesResponse

class HomeViewModel(private val dao: TopGameDao, private val latestDao : LatestGameDao, private val api: ApiInterface) : ViewModel() {
    private var mutableTopGameListLocal: MutableLiveData<Resource<List<GameDbModel>>> =
        MutableLiveData()
    val topGameListLocal: LiveData<Resource<List<GameDbModel>>>
        get() = mutableTopGameListLocal

    private var mutableLatestGameListLocal: MutableLiveData<Resource<List<GameDbModel>>> =
        MutableLiveData()
    val latestGameListLocal: LiveData<Resource<List<GameDbModel>>>
        get() = mutableLatestGameListLocal

    private var mutableTopGameListNetwork: MutableLiveData<Resource<List<GameDbModel>>> = MutableLiveData()
    val topGameListNetwork: LiveData<Resource<List<GameDbModel>>>
        get() = mutableTopGameListNetwork

    private var mutableLatestGameListNetwork: MutableLiveData<Resource<List<GameDbModel>>> = MutableLiveData()
    val latestGameListNetwork: LiveData<Resource<List<GameDbModel>>>
        get() = mutableLatestGameListNetwork

    private val compositeDisposable = CompositeDisposable()

    fun getTopGamesFromLocal() {
        mutableTopGameListLocal.value = Resource.loading()
        compositeDisposable.add(
            dao.getTopGames()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        mutableTopGameListLocal.value = Resource.success(result)
                    },
                    {
                        mutableTopGameListLocal.value = Resource.error(it.localizedMessage)
                    }
                )
        )
    }

    fun getTopGamesFromNetwork(limit: Int, offset: Int) {
        compositeDisposable.add(
            api.fetchTopGames(BuildConfig.BASE_KEY,15,"-rating",4, 1,"2021-12-01,2021-12-31")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        val list = result.results.map { GameDbModel.convertToDbModel(it) }
                        mutableTopGameListNetwork.value = Resource.success(list)
                    },
                    {
                        mutableTopGameListNetwork.value = Resource.error(it.localizedMessage)
                    }
                )
        )
    }


    fun getLatestGamesFromLocal() {
        mutableLatestGameListLocal.value = Resource.loading()
        compositeDisposable.add(
            latestDao.getLatestGames()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        mutableLatestGameListLocal.value = Resource.success(result)
                    },
                    {
                        mutableLatestGameListLocal.value = Resource.error(it.localizedMessage)
                    }
                )
        )
    }


    fun getLatestGamesFromNetwork(limit: Int, offset: Int) {
        compositeDisposable.add(
            api.fetchLatestGames(BuildConfig.BASE_KEY,limit,"-release",4, offset)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->

                        val list = result.results.map {  GameDbModel.convertToDbModel(it)}

                        mutableLatestGameListNetwork.value = Resource.success(list)
                    },
                    {
                        mutableLatestGameListNetwork.value = Resource.error(it.localizedMessage)
                    }
                )
        )
    }




    fun insertToDatabase(games: List<GameDbModel>) {
        compositeDisposable.add(
            dao.insertGames(games).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        )
    }
}