package io.rahman.games.ui

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import io.rahman.games.databinding.ActivitySplashBinding

/**
 * Created              : Rahman on 29/07/2022.
 * Date Created         : 29/07/2022 / 15:57.
 * ===================================================
 * Package              : io.rahman.games.ui
 * Project Name         : GameCatalog.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
class Splash : AppCompatActivity() {

    companion object {
        private const val SPLASH_TIME_OUT = 1500L
    }


    lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)


        with(binding){
            splashLottie.addAnimatorListener(object : Animator.AnimatorListener{
                override fun onAnimationStart(animation: Animator?) {}
                override fun onAnimationEnd(animation: Animator?) {
                    title.visibility = View.VISIBLE

                    Handler(Looper.getMainLooper()).postDelayed({
                        startActivity(Intent(this@Splash, MainActivity::class.java))
                        finish()
                    }, SPLASH_TIME_OUT)
                }

                override fun onAnimationCancel(animation: Animator?) {}

                override fun onAnimationRepeat(animation: Animator?) {}

            })
        }
    }
}