package io.rahman.games.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.rahman.games.R
import io.rahman.games.core.inflate
import io.rahman.games.data.local.model.GameDbModel
import io.rahman.games.databinding.ItemGameBinding

class GameAdapter : RecyclerView.Adapter<GameAdapter.TopGameViewHolder>() {

    var games: MutableList<GameDbModel> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun addGames(list: List<GameDbModel>) {
        games.addAll(list)
        notifyDataSetChanged()
    }

    inner class TopGameViewHolder(private val binding: ItemGameBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun populateModel(game: GameDbModel) {
            binding.apply {
                Glide.with(binding.root.context).load(game.backgroundImage).centerCrop().into(box)
                name.text = game.name
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopGameViewHolder {
        val itemView = parent.inflate(R.layout.item_game)
        val binding = ItemGameBinding.bind(itemView)
        return TopGameViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TopGameViewHolder, position: Int) {
        holder.populateModel(games[position])
    }

    override fun getItemCount() = games.size
}