package io.rahman.games.ui.rate

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import io.rahman.games.R
import io.rahman.games.core.onClick
import io.rahman.games.databinding.FragmentRateBinding

class RateFragment : Fragment(R.layout.fragment_rate) {

    private lateinit var binding: FragmentRateBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRateBinding.bind(view)
        binding.sendFeedback.onClick {
            Toast.makeText(requireContext(), "Feedback sent, thank you!", Toast.LENGTH_SHORT).show()
        }
    }
}