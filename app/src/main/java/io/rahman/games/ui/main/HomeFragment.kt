package io.rahman.games.ui.main

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.koin.androidx.viewmodel.ext.android.viewModel
import io.rahman.games.R
import io.rahman.games.core.ResourceState
import io.rahman.games.core.onClick
import io.rahman.games.core.visibility
import io.rahman.games.databinding.FragmentHomeBinding
import io.rahman.games.ui.adapter.GameAdapter

class HomeFragment : Fragment(R.layout.fragment_home) {

    private val adapterTop = GameAdapter()
    private val adapterLatest = GameAdapter()
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var binding: FragmentHomeBinding
    private lateinit var navController: NavController
    private var isLoading = false
    private val limit = 10
    private var offset = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadData(limit, offset)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeBinding.bind(view)
        navController = Navigation.findNavController(view)
        binding.apply {
            inTopRateRecyclerView.adapter = adapterTop
            val mToplayoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            inTopRateRecyclerView.layoutManager = mToplayoutManager
            inTopRateRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!isLoading) {
                        if (mToplayoutManager.findLastCompletelyVisibleItemPosition() == adapterTop.itemCount - 1) {
                            loadData(limit, offset)
                        }
                    }
                }
            })
            viewModel.topGameListLocal.observe(viewLifecycleOwner) {
                when (it.status) {
                    ResourceState.LOADING -> inTopRateProgressBar.visibility(true)
                    ResourceState.SUCCESS -> {
                        inTopRateProgressBar.visibility(false)
                        adapterTop.games = it.data!!.toMutableList()
                    }
                    ResourceState.ERROR -> {
                        inTopRateProgressBar.visibility(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            viewModel.topGameListNetwork.observe(viewLifecycleOwner) {
                when (it.status) {
                    ResourceState.LOADING -> inTopRateProgressBar.visibility(true)
                    ResourceState.SUCCESS -> {
                        viewModel.insertToDatabase(it.data!!)
                        inTopRateProgressBar.visibility(false)
                        adapterTop.addGames(it.data)
                        offset += 2
                    }
                    ResourceState.ERROR -> {
                        viewModel.getTopGamesFromLocal()
                    }
                }
            }

            //Latest Game
            latestRecyclerView.adapter = adapterLatest
            val mLatestLayoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            latestRecyclerView.layoutManager = mLatestLayoutManager
            latestRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!isLoading) {
                        if (mLatestLayoutManager.findLastCompletelyVisibleItemPosition() == adapterLatest.itemCount - 1) {
                            loadData(limit, offset)
                        }
                    }
                }
            })
            viewModel.latestGameListLocal.observe(viewLifecycleOwner) {
                when (it.status) {
                    ResourceState.LOADING -> inTopRateProgressBar.visibility(true)
                    ResourceState.SUCCESS -> {
                        inTopRateProgressBar.visibility(false)
                        adapterTop.games = it.data!!.toMutableList()
                    }
                    ResourceState.ERROR -> {
                        inTopRateProgressBar.visibility(false)
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            viewModel.latestGameListNetwork.observe(viewLifecycleOwner) {
                when (it.status) {
                    ResourceState.LOADING -> latestProgressBar.visibility(true)
                    ResourceState.SUCCESS -> {
                        viewModel.insertToDatabase(it.data!!)
                        latestProgressBar.visibility(false)
                        adapterLatest.addGames(it.data)
                        offset += 2
                    }
                    ResourceState.ERROR -> {
                        viewModel.latestGameListLocal
                    }
                }
            }

        }


    }

    private fun loadData(limit: Int, offset: Int) {
        viewModel.getTopGamesFromNetwork(limit, offset)

        viewModel.getLatestGamesFromNetwork(limit, offset)
    }
}