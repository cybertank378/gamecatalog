package io.rahman.games.data.remote.model

import com.google.gson.annotations.SerializedName

data class TopGameResponse(
    @field:SerializedName("results")
    val results: List<GamesResponse>,

    @field:SerializedName("next")
    val next: String?,

    @field:SerializedName("prev")
    val prev: String?
)