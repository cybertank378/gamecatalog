package io.rahman.games.data.remote

import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import io.rahman.games.BuildConfig
import io.rahman.games.data.remote.model.LatestGameResponse
import io.rahman.games.data.remote.model.TopGameResponse

interface ApiInterface {
    @GET("games")
    fun fetchTopGames(
        @Query("key") key:String?,
        @Query("page_size") pageSize:Int? = 10,
        @Query("ordering") ordering:String? = "-released",
        @Query("platforms") platforms:Int? = 4,
        @Query("page") pageNum :Int?,
        @Query("dates") dates:String?
    ): Observable<TopGameResponse>

    @GET("games")
    fun fetchLatestGames(
        @Query("key") key:String?,
        @Query("page_size") pageSize:Int? = 10,
        @Query("ordering") ordering:String? = "-released",
        @Query("platforms") platforms:Int? = 4,
        @Query("page") pageNum :Int?
    ): Observable<LatestGameResponse>
}