package io.rahman.games.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.rahman.games.data.local.model.GameDbModel

/**
 * Created              : Rahman on 29/07/2022.
 * Date Created         : 29/07/2022 / 15:40.
 * ===================================================
 * Package              : io.rahman.games.data.local
 * Project Name         : GameCatalog.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
@Dao
interface LatestGameDao {
    @Query("SELECT * FROM gamesEntity")
    fun getLatestGames(): Observable<List<GameDbModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGames(games: List<GameDbModel>): Completable
}