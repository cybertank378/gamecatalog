package io.rahman.games.data.local.model

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.room.Entity
import androidx.room.PrimaryKey
import io.rahman.games.data.remote.model.GamesResponse

@Entity(tableName = "gamesEntity")
data class GameDbModel(
    @Nullable
    val suggestionsCount: Int?,

    @Nullable
    val rating: Double?,

    @Nullable
    val playtime: Int?,

    @Nullable
    val backgroundImage: String?,

    @NonNull
    val name: String,

    @PrimaryKey
    @NonNull
    val gamesId: Int,

    @Nullable
    val released: String?,

    @Nullable
    val added: Int?

) {
    companion object {
        fun convertToDbModel(game: GamesResponse): GameDbModel {
            return GameDbModel(
                suggestionsCount = game.suggestionsCount,
                rating = game.rating,
                playtime= game.playtime,
                backgroundImage= game.backgroundImage,
                name= game.name,
                gamesId= game.gamesId,
                released= game.released,
                added= game.added
            )
        }
    }
}