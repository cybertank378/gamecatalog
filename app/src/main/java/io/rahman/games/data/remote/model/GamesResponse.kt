package io.rahman.games.data.remote.model

import com.google.gson.annotations.SerializedName

/**
 * Created              : Rahman on 29/07/2022.
 * Date Created         : 29/07/2022 / 13:31.
 * ===================================================
 * Package              : io.rahman.games.data.remote.model
 * Project Name         : GameCatalog.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
data class GamesResponse(
    @field:SerializedName("suggestions_count")
    val suggestionsCount: Int,

    @field:SerializedName("rating")
    val rating: Double,

    @field:SerializedName("playtime")
    val playtime: Int,

    @field:SerializedName("background_image")
    val backgroundImage: String,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("id")
    val gamesId: Int,

    @field:SerializedName("released")
    val released: String,

    @field:SerializedName("added")
    val added: Int
)