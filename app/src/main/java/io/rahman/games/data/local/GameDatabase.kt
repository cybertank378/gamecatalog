package io.rahman.games.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import io.rahman.games.data.local.model.GameDbModel

@Database(entities = [GameDbModel::class], version =2)
abstract class GameDatabase : RoomDatabase() {
    abstract fun topGameDao() : TopGameDao
    abstract fun latestGameDao() : LatestGameDao



}